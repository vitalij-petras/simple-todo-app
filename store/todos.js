export const state = () => ({
  list: [],
  removeId: null,
})

export const getters = {
  todosNotDone({ list }) {
    return list.filter(({ done }) => done === false).sort((a, b) => b.id - a.id)
  },
  todosDone({ list }) {
    return list.filter(({ done }) => done !== false).sort((a, b) => b.id - a.id)
  },
}

export const actions = {
  add({ commit, dispatch }, text) {
    commit('add', text)
    dispatch('saveLocalStorage')
  },
  toggle({ state, commit, dispatch }, todoId) {
    const todo = state.list.find(({ id }) => id === todoId)
    if (todo) {
      commit('toggle', todo)
      dispatch('saveLocalStorage')
    }
  },
  changeRemoveId({ commit }, todoId = null) {
    commit('changeRemoveId', todoId)
  },
  removeAccept({ commit, state, dispatch }) {
    commit('remove', state.removeId)
    dispatch('saveLocalStorage')
  },
  loadTodosFromLocalStorage({ commit }) {
    const todoList = localStorage.getItem('todolist')
    if (todoList) {
      commit('loadTodosFromLocalStorage', JSON.parse(todoList))
    }
  },
  saveLocalStorage({ state }) {
    localStorage.setItem('todolist', JSON.stringify(state.list))
  },
  clearLocalStorage() {
    localStorage.clear()
  },
  findById({ state }, todoId) {
    return state.list.find(({ id }) => id === todoId)
  },
  edit({ state, commit, dispatch }, { newText, todoId }) {
    const todo = state.list.find(({ id }) => id === todoId)
    if (todo) {
      commit('edit', { newText, todo })
      dispatch('saveLocalStorage')
    }
  },
}

export const mutations = {
  add(state, text) {
    state.list.push({
      id: state.list.length > 0 ? state.list[state.list.length - 1].id + 1 : 0,
      text,
      done: false,
    })
  },
  remove(state, todoId) {
    state.list = state.list.filter(({ id }) => id !== todoId)
  },
  toggle(state, todo) {
    todo.done = !todo.done
  },
  edit(state, { newText, todo }) {
    todo.text = newText
  },
  changeRemoveId(state, newId) {
    state.removeId = newId
  },
  loadTodosFromLocalStorage(state, todoList) {
    state.list = todoList
  },
}
