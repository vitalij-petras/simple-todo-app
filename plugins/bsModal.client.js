import Modal from '~/node_modules/bootstrap/js/src/modal'

export default function (context, inject) {
  inject('bsModal', {
    open,
  })

  function init(el, options) {
    return new Modal(el, options)
  }

  function open(elementId = 'modal-remove', options = {}) {
    const element = document.getElementById(elementId)
    if (element) {
      const modalObject = init(element, options)
      modalObject.show()
    }
  }
}
